import { Injectable } from '@angular/core';
//import { Message } from '_debugger';

@Injectable()
export class MessageService {
  message: string[] = [];

  add(message: string) {
    this.message.push(message);
  }

  clear() {
    this.message.length = 0;
  }
  
  constructor() { }

  constructor() { }

}
