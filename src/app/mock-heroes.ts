import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Mr. Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Superman' },
  { id: 16, name: 'Batman' },
  { id: 17, name: 'Antman' },
  { id: 18, name: 'Thor' },
  { id: 19, name: 'Wonder women' },
  { id: 20, name: 'Hulk' },

];